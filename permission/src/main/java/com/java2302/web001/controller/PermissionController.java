package com.java2302.web001.controller;

import com.java2302.web001.entity.PermissionEntity;
import com.java2302.web001.service.PermissionService;
import com.java2302.web001.vo.PermissionVo;
import com.java2302.web001.vo.ResultVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
@CrossOrigin
@RestController
@RequestMapping("permission")
public class PermissionController {
    @Resource
    PermissionService permissionService;

    @GetMapping("list")
    public ResultVo list(){
        return ResultVo.success("查询成功",permissionService.findAll());
    }

    @PostMapping("add")
    public ResultVo add(PermissionEntity permissionEntity){
        permissionService.save(permissionEntity);
        return ResultVo.success("添加成功",null);
    }

    @PutMapping("update")
    public ResultVo update(PermissionEntity permissionEntity){
        permissionService.update(permissionEntity);
        return ResultVo.success("修改成功",null);
    }

    @DeleteMapping("delete")
    public ResultVo delete(Integer id){
        System.out.println(id);
        permissionService.delete(id);
        return ResultVo.success("删除成功",null);
    }

}
