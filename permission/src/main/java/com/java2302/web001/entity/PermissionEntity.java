package com.java2302.web001.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Table("tb_permission")
public class PermissionEntity {
    @Id(keyType = KeyType.Auto)
    @Column("permission_id")
    Integer permissionId;
    @Column("permission_name")
    String permissionName;
}
