package com.java2302.web001.service;

import com.java2302.web001.entity.PermissionEntity;
import com.java2302.web001.mapper.PermissionsMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PermissionService {
    @Resource
    PermissionsMapper permissionsMapper;

    public List<PermissionEntity> findAll() {
        return permissionsMapper.selectAll();
    }

    public void delete(Integer id){
        permissionsMapper.deleteById(id);
    }

    public void save(PermissionEntity permissionEntity) {
        permissionsMapper.insert(permissionEntity);
    }

    public void update(PermissionEntity permissionEntity) {
        permissionsMapper.update(permissionEntity);
    }








}
