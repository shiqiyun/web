package com.java2302.web001.controller;

import com.java2302.web001.service.PremissionsService;
import com.java2302.web001.vo.PermissionsVo;
import com.java2302.web001.vo.ResultsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/
@CrossOrigin
@RestController
@RequestMapping("getPermission")
public class PermissionsController {
    @Autowired
    PremissionsService premissionsService;

    @GetMapping("get")
    public ResultsVo getPermission(){
        List<PermissionsVo> list = premissionsService.list();
        return ResultsVo.success("查询成功", list, null);
    }
}
