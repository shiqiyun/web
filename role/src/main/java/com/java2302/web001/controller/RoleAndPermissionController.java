package com.java2302.web001.controller;

import com.java2302.web001.service.RoleAndPermissionService;
import com.java2302.web001.vo.PermissionsVo;
import com.java2302.web001.vo.ResultsVo;
import com.java2302.web001.vo.RoleAndPermissionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/
@CrossOrigin
@RestController
@RequestMapping("roleAndPermission")
public class RoleAndPermissionController {
    @Autowired
    RoleAndPermissionService roleAndPermissionService;

    @PostMapping("list")
    public ResultsVo list(Integer id) {
        List<RoleAndPermissionVo> list = roleAndPermissionService.list(id);
        return ResultsVo.success("查询成功", list, null);
    }

    @PostMapping("add")
    public ResultsVo add(Integer id, @RequestBody PermissionsVo permissionsVo) {
        roleAndPermissionService.add(id, permissionsVo);
        return ResultsVo.success("添加成功", null, null);
    }

    @DeleteMapping("del")
    public ResultsVo delete(Integer id, @RequestBody PermissionsVo permissionsVo) {
        roleAndPermissionService.delete(id, permissionsVo);
        return ResultsVo.success("删除成功", null, null);
    }
}
