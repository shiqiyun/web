package com.java2302.web001.controller;

import com.java2302.web001.service.RoleService;
import com.java2302.web001.vo.PageVo;
import com.java2302.web001.vo.ResultsVo;
import com.java2302.web001.vo.RoleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/
@CrossOrigin
@RestController
@RequestMapping("role")
public class RoleController {
    @Autowired
    RoleService roleService;

    //分页查询角色
    @PostMapping
    public ResultsVo getRole(PageVo pageVo) {
        Map<String,Object> map = roleService.findPage(pageVo);
        return ResultsVo.success("查询成功", map, null);
    }

    //显示所有角色
    @GetMapping("list")
    public ResultsVo getRole() {
        List<RoleVo> list = roleService.list();
        return ResultsVo.success("查询成功", list, null);
    }

    //添加角色
    @PostMapping("add")
    public ResultsVo add(@RequestBody RoleVo roleVo) {
        roleService.add(roleVo);
        return ResultsVo.success("添加成功", null, null);
    }
    //刪除角色
    @DeleteMapping("del")
    public ResultsVo delete(Integer roleId){
        System.out.println(roleId);
        roleService.delete(roleId);
        return ResultsVo.success("删除成功",null,null);
    }

    //修改角色
    @PutMapping("update")
    public ResultsVo update(@RequestBody RoleVo roleVo){
        roleService.update(roleVo);
        return ResultsVo.success("修改成功",null,null);
    }
}
