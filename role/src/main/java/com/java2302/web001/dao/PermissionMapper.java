package com.java2302.web001.dao;

import com.java2302.web001.entity.PermissionEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/
@Mapper
public interface PermissionMapper extends BaseMapper<PermissionEntity>{

}
