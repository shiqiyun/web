package com.java2302.web001.dao;

import com.java2302.web001.entity.RoleAndPermissionEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/
@Mapper
public interface RoleAndPermissionMapper extends BaseMapper<RoleAndPermissionEntity> {
}
