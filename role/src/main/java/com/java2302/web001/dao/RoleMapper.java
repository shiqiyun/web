package com.java2302.web001.dao;


import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.java2302.web001.entity.RoleEntity;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/
@Mapper
public interface RoleMapper extends BaseMapper<RoleEntity> {
}
