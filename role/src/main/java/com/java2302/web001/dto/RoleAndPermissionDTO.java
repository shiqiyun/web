package com.java2302.web001.dto;

import com.java2302.web001.entity.PermissionEntity;
import com.java2302.web001.entity.RoleAndPermissionEntity;
import com.java2302.web001.entity.RoleEntity;
import lombok.Data;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/
@Data
public class RoleAndPermissionDTO extends RoleAndPermissionEntity {
    PermissionEntity permissionEntity;
    RoleEntity roleEntity;
}
