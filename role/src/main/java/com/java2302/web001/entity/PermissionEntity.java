package com.java2302.web001.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import lombok.Data;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/
@Data
@Table("tb_permission")
public class PermissionEntity {
    @Id(keyType = KeyType.Auto)
    Integer permissionId;

    @Column
    String permissionName;
}
