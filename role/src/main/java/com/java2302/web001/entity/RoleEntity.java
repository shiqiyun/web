package com.java2302.web001.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import lombok.Data;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/

@Data
@Table("tb_role")
public class RoleEntity {
    @Id(keyType = KeyType.Auto)
    Integer roleId;

    @Column
    String roleName;

}
