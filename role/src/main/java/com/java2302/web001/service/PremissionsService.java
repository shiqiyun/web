package com.java2302.web001.service;

import com.java2302.web001.dao.PermissionMapper;
import com.java2302.web001.entity.PermissionEntity;
import com.java2302.web001.vo.PermissionsVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/
@Service
public class PremissionsService {
    @Resource
    PermissionMapper permissionMapper;

    public List<PermissionsVo> list(){
        List<PermissionEntity> list =permissionMapper.selectAll();
        List<PermissionsVo> list1 = new java.util.ArrayList<>();
        for (PermissionEntity permissionEntity : list) {
            PermissionsVo permissionsVo = new PermissionsVo();
            permissionsVo.setPermissionId(permissionEntity.getPermissionId());
            permissionsVo.setPermissionName(permissionEntity.getPermissionName());
            list1.add(permissionsVo);
        }
        return list1;
    }
}
