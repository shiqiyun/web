package com.java2302.web001.service;

import com.java2302.web001.dao.RoleAndPermissionMapper;
import com.java2302.web001.entity.RoleAndPermissionEntity;
import com.java2302.web001.vo.PermissionsVo;
import com.java2302.web001.vo.RoleAndPermissionVo;
import com.mybatisflex.core.query.QueryWrapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/
@Service
public class RoleAndPermissionService {
    @Resource
    RoleAndPermissionMapper roleAndPermissionMapper;

    public List<RoleAndPermissionVo> list(Integer id){
        List<RoleAndPermissionEntity> list =roleAndPermissionMapper.selectListByQuery(new QueryWrapper().where("role_id = ?",id));
        List<RoleAndPermissionVo> list1 = new java.util.ArrayList<>();
        for (RoleAndPermissionEntity roleAndPermissionEntity : list) {
            RoleAndPermissionVo roleAndPermissionVo = new RoleAndPermissionVo();
            roleAndPermissionVo.setId(roleAndPermissionEntity.getId());
            roleAndPermissionVo.setRoleId(roleAndPermissionEntity.getRoleId());
            roleAndPermissionVo.setPermissionId(roleAndPermissionEntity.getPermissionId());
            list1.add(roleAndPermissionVo);
        }
        return list1;
    }

    public void add(Integer id, PermissionsVo permissionsVo){
        RoleAndPermissionEntity roleAndPermissionEntity = new RoleAndPermissionEntity();
        roleAndPermissionEntity.setPermissionId(permissionsVo.getPermissionId());
        roleAndPermissionEntity.setRoleId(id);
        roleAndPermissionMapper.insert(roleAndPermissionEntity);
    }

    public void delete(Integer id, PermissionsVo permissionsVo){
        roleAndPermissionMapper.deleteByQuery(new QueryWrapper().where("role_id = #{id} and permission_id = #{permissionId}",id, permissionsVo.getPermissionId()));
    }
}
