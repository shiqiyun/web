package com.java2302.web001.service;

import com.java2302.web001.dao.RoleMapper;
import com.java2302.web001.entity.RoleEntity;
import com.java2302.web001.vo.PageVo;
import com.java2302.web001.vo.RoleVo;
import com.mybatisflex.core.query.QueryWrapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/
@Service
public class RoleService {
    @Resource
    RoleMapper roleMapper;

    //分页查询
    public Map<String,Object> findPage(PageVo pageVo){
        QueryWrapper query = new QueryWrapper()
                .from(RoleEntity.class)
                .limit(pageVo.getPageIndex()-1,pageVo.getPageSize());

        List<RoleEntity> list = roleMapper.selectListByQuery(query);
        List<RoleEntity> list1 =roleMapper.selectAll();

        Map<String,Object> map = new HashMap<>();
        map.put("list",list);
        map.put("total",list1.size());
        return map;
    }


    public List<RoleVo> list(){
        List<RoleEntity> list = roleMapper.selectAll();
        List<RoleVo> list1 = new ArrayList<>();
        for (RoleEntity roleEntity : list) {
            RoleVo roleVo = new RoleVo();
            roleVo.setRoleId(roleEntity.getRoleId());
            roleVo.setRoleName(roleEntity.getRoleName());
            list1.add(roleVo);
        }
        return list1;
    }

    public void add(RoleVo roleVo){
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setRoleName(roleVo.getRoleName());
        roleMapper.insert(roleEntity);
    }

    public void delete(Integer roleId){
        roleMapper.deleteByQuery(new QueryWrapper().where("role_id=?",roleId));
    }

    public void update(RoleVo roleVo){
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setRoleId(roleVo.getRoleId());
        roleEntity.setRoleName(roleVo.getRoleName());
        roleMapper.update(roleEntity);
    }
}
