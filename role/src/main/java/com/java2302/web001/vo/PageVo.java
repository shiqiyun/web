package com.java2302.web001.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author: summer
 * @Created: 2023/7/29
 **/
@Getter
@Setter
public class PageVo {
    Integer pageIndex;
    Integer pageSize;
}
