package com.java2302.web001.vo;


import lombok.Getter;
import lombok.Setter;

/**
 * @Author: summer
 * @Created: 2023/7/27
 **/
@Setter
@Getter
public class ResultsVo {
    public static final int SUCCESS = 200;
    public static final int ERROR = 400;
    public static final int NOAUTH = 401;

    Integer code;
    String msg;
    Object data;
    Long count;

    public ResultsVo() {
    }

    public static ResultsVo success(String msg, Object data, Long count) {
        ResultsVo resultsVo = new ResultsVo();
        resultsVo.setCode(SUCCESS);
        resultsVo.setMsg(msg);
        resultsVo.setData(data);
        resultsVo.setCount(count);
        return resultsVo;
    }

    public static ResultsVo fail(String msg, Object data, Long count) {
        ResultsVo resultsVo = new ResultsVo();
        resultsVo.setCode(ERROR);
        resultsVo.setMsg(msg);
        resultsVo.setData(data);
        resultsVo.setCount(count);
        return resultsVo;
    }
}
