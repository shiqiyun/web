package com.java2302.web001.vo;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import lombok.Data;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/
@Data
public class RoleVo {
    Integer roleId;
    String roleName;
}
