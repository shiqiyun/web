package com.java2302.web001;

import com.java2302.web001.dao.RoleAndPermissionMapper;
import com.java2302.web001.dto.RoleAndPermissionDTO;
import com.java2302.web001.entity.PermissionEntity;
import com.java2302.web001.entity.RoleAndPermissionEntity;
import com.java2302.web001.entity.RoleEntity;
import com.mybatisflex.core.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @Author: summer
 * @Created: 2023/8/8
 **/


class Web001ApplicationTest {

    @Resource
    RoleAndPermissionMapper roleAndPermissionMapper;

    @Test
    void test() {
        QueryWrapper query = new QueryWrapper()
                .from(RoleAndPermissionEntity.class,RoleEntity.class, PermissionEntity.class)
                .where("roleAndPermissionEntity.roleId = roleEntity.id")
                .and("roleAndPermissionEntity.permissionId = permissionEntity.id");


        RoleAndPermissionDTO roleAndPermissionDTO = roleAndPermissionMapper.selectOneByQueryAs(query, RoleAndPermissionDTO.class);
        System.out.println(roleAndPermissionDTO);

    }
}
