package com.java2302.web001.entity.table;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

// Auto generate by mybatis-flex, do not modify it.
public class PermissionEntityTableDef extends TableDef {

    /**
     * @Author: summer
 @Created: 2023/8/8
     */
    public static final PermissionEntityTableDef PERMISSION_ENTITY = new PermissionEntityTableDef();

    public final QueryColumn PERMISSION_ID = new QueryColumn(this, "permission_id");

    public final QueryColumn PERMISSION_NAME = new QueryColumn(this, "permission_name");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{PERMISSION_ID, PERMISSION_NAME};

    public PermissionEntityTableDef() {
        super("", "tb_permission");
    }

}
