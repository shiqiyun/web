package com.java2302.web001.entity.table;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

// Auto generate by mybatis-flex, do not modify it.
public class RoleEntityTableDef extends TableDef {

    /**
     * @Author: summer
 @Created: 2023/8/8
     */
    public static final RoleEntityTableDef ROLE_ENTITY = new RoleEntityTableDef();

    public final QueryColumn ROLE_ID = new QueryColumn(this, "role_id");

    public final QueryColumn ROLE_NAME = new QueryColumn(this, "role_name");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{ROLE_ID, ROLE_NAME};

    public RoleEntityTableDef() {
        super("", "tb_role");
    }

}
