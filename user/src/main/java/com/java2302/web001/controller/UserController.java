package com.java2302.web001.controller;

import com.java2302.web001.service.UserService;
import com.java2302.web001.vo.QueryVo;
import com.java2302.web001.vo.ResultVo;
import com.java2302.web001.vo.UserVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("user")
@CrossOrigin
public class UserController {
    @Resource
    UserService userService;

    @GetMapping("list")
    public ResultVo list(QueryVo queryVo){
        return ResultVo.success("查询成功",userService.findAllPage(queryVo));
    }

    @GetMapping("list1")
    public ResultVo list1(){
        return ResultVo.success("查询成功",userService.findAll());
    }

    @PostMapping("add")
    public ResultVo add(UserVo userVo){
        userService.save(userVo);
        return ResultVo.success("添加成功",null);
    }
    @PutMapping("update")
    public ResultVo update(UserVo userVo){
        userService.update(userVo);
        return ResultVo.success("修改成功",null);
    }
    @DeleteMapping("delete")
    public ResultVo delete(Integer userId){
        userService.delete(userId);
        return ResultVo.success("删除成功",null);
    }

}
