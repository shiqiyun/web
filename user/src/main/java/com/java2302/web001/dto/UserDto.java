package com.java2302.web001.dto;

import com.java2302.web001.entity.UserPojo;
import lombok.Data;

@Data
public class UserDto extends UserPojo {
    Integer id;
    Integer roleId;
    String roleName;
}
