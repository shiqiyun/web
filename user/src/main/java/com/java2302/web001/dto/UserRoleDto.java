package com.java2302.web001.dto;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Table("tb_user_role")
@AllArgsConstructor
@NoArgsConstructor
public class UserRoleDto {
    @Id(keyType = KeyType.Auto)
    Integer id;
    @Column
    Integer userId;
    @Column
    Integer roleId;
}
