package com.java2302.web001.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Table("tb_user")
@AllArgsConstructor
@NoArgsConstructor
public class UserPojo {
    @Id(keyType = KeyType.Auto)
    Integer userId;
    @Column
    String userName;
}
