package com.java2302.web001.mapper;

import com.java2302.web001.entity.UserPojo;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<UserPojo> {
}
