package com.java2302.web001.service;

import com.java2302.web001.dto.UserDto;
import com.java2302.web001.dto.UserRoleDto;
import com.java2302.web001.entity.RolePojo;
import com.java2302.web001.entity.UserPojo;
import com.java2302.web001.mapper.RolesMapper;
import com.java2302.web001.mapper.UserMapper;
import com.java2302.web001.mapper.UserRoleMapper;
import com.java2302.web001.vo.QueryVo;
import com.java2302.web001.vo.UserVo;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.java2302.web001.dto.table.UserRoleDtoTableDef.USER_ROLE_DTO;
import static com.java2302.web001.entity.table.RolePojoTableDef.ROLE_POJO;
import static com.java2302.web001.entity.table.UserPojoTableDef.USER_POJO;

@Service
public class UserService {
    @Resource
    RolesMapper rolesMapper;
    @Resource
    UserMapper userMapper;
    @Resource
    UserRoleMapper userRoleMapper;

    public Map<String,Object> findAllPage(QueryVo queryVo) {
        Page<UserDto> page = userMapper.paginateAs(queryVo.getPageIndex(),queryVo.getPageSize(),
                new QueryWrapper().select(
                        USER_POJO.USER_ID.as("userId"),
                        USER_POJO.USER_NAME.as("userName"),
                        USER_ROLE_DTO.ID.as("id"),
                        ROLE_POJO.ROLE_ID.as("roleId"),
                        ROLE_POJO.ROLE_NAME.as("roleName")
                ).from(USER_POJO,USER_ROLE_DTO,ROLE_POJO)
                        .where(USER_POJO.USER_ID.eq(USER_ROLE_DTO.USER_ID)
                                .and(USER_ROLE_DTO.ROLE_ID.eq(ROLE_POJO.ROLE_ID))
                ),UserDto.class);
        List<UserDto> list = page.getRecords();
        Long total = page.getTotalRow();
        Map<String,Object> map = new HashMap<>();
        map.put("list",list);
        map.put("total",total);
        return map;
    }

    public List<RolePojo> findAll(){
        return rolesMapper.selectListByQuery(new QueryWrapper());
    }

    public void save(UserVo userVo) {
        UserPojo userPojo = new UserPojo(null, userVo.getUserName());
        userMapper.insert(userPojo);
        UserPojo userPojo1 = userMapper.selectOneByQuery(new QueryWrapper().where("user_name=?", userVo.getUserName()));
        userRoleMapper.insert(new UserRoleDto(null,userPojo1.getUserId(),userVo.getRoleId()));
    }

    public void update(UserVo userVo){
        userRoleMapper.update(new UserRoleDto(userVo.getId(),userVo.getUserId(),userVo.getRoleId()));
    }

    public void delete(Integer userId){
        UserRoleDto userRoleDto = userRoleMapper.selectOneByQuery(new QueryWrapper().where("user_id=?",userId));
        userRoleMapper.deleteById(userRoleDto.getId());
        userMapper.deleteById(userId);
    }


}
