package com.java2302.web001.vo;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class QueryVo {
    Integer pageIndex;
    Integer pageSize;
}
