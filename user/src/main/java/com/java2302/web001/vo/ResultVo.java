package com.java2302.web001.vo;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResultVo {
    Integer code;
    String msg;
    Object data;

    public static ResultVo success(String msg,Object data){
        ResultVo resultVo = new ResultVo();
        resultVo.setCode(200);
        resultVo.setMsg(msg);
        resultVo.setData(data);
        return resultVo;
    }

    public static ResultVo fail(String msg,Object data){
        ResultVo resultVo = new ResultVo();
        resultVo.setCode(400);
        resultVo.setMsg(msg);
        resultVo.setData(data);
        return resultVo;
    }
}
