package com.java2302.web001.vo;

import lombok.Data;

@Data
public class UserVo {
    Integer id;
    Integer userId;
    String userName;
    Integer roleId;
}
