package com.java2302.web001.entity.table;

import com.mybatisflex.core.query.QueryColumn;
import com.mybatisflex.core.table.TableDef;

// Auto generate by mybatis-flex, do not modify it.
public class UserPojoTableDef extends TableDef {

    public static final UserPojoTableDef USER_POJO = new UserPojoTableDef();

    public final QueryColumn USER_ID = new QueryColumn(this, "user_id");

    public final QueryColumn USER_NAME = new QueryColumn(this, "user_name");

    /**
     * 所有字段。
     */
    public final QueryColumn ALL_COLUMNS = new QueryColumn(this, "*");

    /**
     * 默认字段，不包含逻辑删除或者 large 等字段。
     */
    public final QueryColumn[] DEFAULT_COLUMNS = new QueryColumn[]{USER_ID, USER_NAME};

    public UserPojoTableDef() {
        super("", "tb_user");
    }

}
